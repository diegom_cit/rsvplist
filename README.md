# README #

### What is this repository for? ###

* This module is used to Collect emails from users that wants to subscribe to an specific content on your site.
* Version 1.0

### How do I get set up? ###

#### Dependencies ####

* Drupal 8/9 Instalation
* Git

#### Step by Step ####

1. Clone this module at your modules folder
	* `$ cd /docroot/modules/custom`
	* `$ git clone git@bitbucket.org:diegom_cit/rsvplist.git rsvplist`
1. Log in into your Drupal as Admin
	* Go to any login block and/or url */user/login*
	* Type your user and password
1. Enable RSVP List module
	* Go to menu **Extend**
	* Search for *RSVP List*
	* Instal module
1. Go to RSVP Settings to enable which content types can show this block
	* Go to url */admin/config/content/rsvplist*
	* Flag which contents do you want this block enabled
	* Click on save
1. Place your block somewhere at the page
	* Go to menu **Strucuture -> Block Layout**
	* Choose a region and click on add (example: **Sidebar First**)
	* Click on **Add Block** than click on **Save**
1. At each node, Enable RSVP List subscriptions.
	* Open desired Node and edit it
	* At *Tabs Menu*, go to **RSVP List** and enable it
	* Publish your node

### Who do I talk to? ###

* Diego Ocko Martins: diegom@ciandt.com